USE [master]
GO
/****** Object:  Database [KermanyDb]    Script Date: 12/30/2021 3:41:56 PM ******/
CREATE DATABASE [KermanyDb] ON  PRIMARY 
( NAME = N'KermanyDb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\KermanyDb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'KermanyDb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\KermanyDb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [KermanyDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [KermanyDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [KermanyDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [KermanyDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [KermanyDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [KermanyDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [KermanyDb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [KermanyDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [KermanyDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [KermanyDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [KermanyDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [KermanyDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [KermanyDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [KermanyDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [KermanyDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [KermanyDb] SET  ENABLE_BROKER 
GO
ALTER DATABASE [KermanyDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [KermanyDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [KermanyDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [KermanyDb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [KermanyDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [KermanyDb] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [KermanyDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [KermanyDb] SET RECOVERY FULL 
GO
ALTER DATABASE [KermanyDb] SET  MULTI_USER 
GO
ALTER DATABASE [KermanyDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [KermanyDb] SET DB_CHAINING OFF 
GO
EXEC sys.sp_db_vardecimal_storage_format N'KermanyDb', N'ON'
GO
USE [KermanyDb]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 12/30/2021 3:41:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Article]    Script Date: 12/30/2021 3:41:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Article](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Type] [int] NOT NULL,
	[CreateDate] [datetime2](7) NOT NULL,
	[ImageName] [nvarchar](max) NOT NULL,
	[VisitCount] [int] NOT NULL,
 CONSTRAINT [PK_Tbl_Article] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_ArticleCategory]    Script Date: 12/30/2021 3:41:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_ArticleCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Tbl_ArticleCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Slider]    Script Date: 12/30/2021 3:41:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Slider](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Imagename] [nvarchar](max) NOT NULL,
	[Url] [nvarchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Tbl_Slider] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_User]    Script Date: 12/30/2021 3:41:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](max) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[Role] [int] NOT NULL,
	[CreateDate] [datetime2](7) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Tbl_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20211228150625_initial', N'6.0.0')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20211229205319_slider', N'6.0.1')
GO
SET IDENTITY_INSERT [dbo].[Tbl_Article] ON 

INSERT [dbo].[Tbl_Article] ([Id], [CategoryId], [Title], [Description], [Text], [Type], [CreateDate], [ImageName], [VisitCount]) VALUES (1002, 1002, N'قند میوه ها: جدول و میزان قند انواع میوه؛ چرا باید مراقب قند میوه باشید!', N'میوه‌ها گروه غذایی‌اند که استفاده‌شان برای افراد دیابتی یا کسانی گیج کننده است که کم کربوهیدرات استفاده می‌کنند. شاید شنیده باشید که می‌گویند نباید نگران میزان قند میوه‌ ها بود، چون یکی از مواد طبیعی آن است. اینکه باید نگران قند میوه ها باشید یا نه بستگی دارد. مسلما میوه‌ها سرشار از مواد مغذی‌اند. اگر قرار است قند دریافت کنید، بهترین کار جذب آن به همراه مقدار زیادی ماده مغذی دیگر است. به همین دلیل جدول قند میوه ها را برای‌تان آوردیم. در آخر مقاله نیز می‌خواهیم درباره شاخص گلیسیمی صحبت کنیم.', N'<h2>قند میوه ها را دست کم نگیریم!</h2>

<p>مراقبت از میزان قند دریافتی ایده خوبی است؛ اما کنترل هوسمان به قند، کار آسانی نیست. ممکن است همین حالا هم قندهای فرآوری شده را کنار گذاشته باشید؛ اما ندانید چه مقدار قند در میوه&zwnj;ها نهفته است.</p>

<p>شاید هم به دیابت مبتلایید و دوست دارید بدانید کدام میوه&zwnj;ها کمترین تاثیر را روی قند خونتان دارند. با وجود اینکه میوه حاوی مقدار زیادی از دیگر مواد مغذی است اما برخی از انواع آن نسبت به دیگر میوه&zwnj;ها، قند بیشتری در خود دارند. در ادامه می&zwnj;خواهیم بهترین میوه&zwnj;هایی را که قند کمی دارند به شما معرفی کنیم.</p>

<p><img alt="قند میوه‌ ها بالاست اما مواد مغذی بسیاری نیز دارند. " src="https://kermany.com/wp-content/uploads/2021/12/44444.jpg" style="height:500px; width:750px" /></p>

<p>میوه&zwnj;ها مواد مغذی بسیاری دارند.</p>

<p>&nbsp;</p>

<h3>۱- لیموها</h3>

<p>لیموها سرشار از&nbsp;<strong>ویتامین C&zwnj;</strong>اند و طعمشان ترش است. این میوه&zwnj;ها قند زیادی در خود ندارند. هر لیمو تقریبا&nbsp;<strong>یک یا دو گرم</strong>. می&zwnj;توانید با اضافه کردن یکی از آن&zwnj;ها به لیوان آبتان، به&nbsp;<strong>کاهش اشتها</strong>&nbsp;کمک کنید.</p>

<p>&nbsp;</p>

<h3>۲- توت فرنگی</h3>

<p>توت فرنگی قند بسیاری پایینی دارد؛ اما طعم آن به شکل عجیبی شیرین و دلچسب است. یک فنجان توت فرنگی خام،&nbsp;<strong>هفت گرم قند&nbsp;</strong>دارد که این مقدار توت فرنگی بیش از ۱۰۰ درصد نیاز روزانه بدن به&nbsp;<strong>ویتامین C</strong>&nbsp;را تامین می&zwnj;کند.</p>

<p>&nbsp;</p>

<h3>۳- توت سیاه</h3>

<p>توت سیاه نیز تنها در هر فنجان&nbsp;<strong>هفت گرم قند&nbsp;</strong>دارد. بنابراین با خیال راحت از این توت&zwnj;های سیاه رنگ برای میان وعده میل کنید. توت سیاه سرشار از&nbsp;<strong>آنتی&zwnj;اکسیدان</strong>&nbsp;و&nbsp;<strong>فیبر</strong>&nbsp;نیز است.</p>

<p>&nbsp;</p>

<h3>۴- گریپ فروت</h3>

<p>یکی دیگر از مرکبات که در این لیست وجود دارد، گریپ فروت است. با اینکه شیرینی گریپ فروت به اندازه انگور نیست؛ می&zwnj;تواند انتخاب مناسبی برای صبحانه باشد. نیمی از یک گریپ فروت متوسط،<strong>&nbsp;۹ گرم قند</strong>&nbsp;در خود دارد.</p>

<p><img alt="" src="https://kermany.com/wp-content/uploads/2021/12/333.jpg" style="height:500px; width:750px" /></p>

<p>مرکبات در کنار داشتن قند پایین، به وفور ویتامین C دارند.</p>

<p>&nbsp;</p>

<h3>۵- آووکادو</h3>

<p>وقتی به میوه فکر می&zwnj;کنید، مسلما اولین چیزی که به ذهنتان می&zwnj;آید آووکادو نیست؛ اما آن&zwnj;ها هم میوه&zwnj;اند.&nbsp;<strong>قند بسیار پایینی</strong>&nbsp;نیز دارند. یک آووکادو کامل و خام، تنها&nbsp;<strong>یک گرم قند</strong>&nbsp;در خود دارد. آووکادو سرشار از&nbsp;<strong>چربی&zwnj;های سالمی</strong>&nbsp;است که کمک می&zwnj;کند احساس سیری پایداری داشته باشید</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h3>۶- هندوانه</h3>

<p>هندوانه یکی از میوه&zwnj;های شاخص تابستان است.&nbsp;<strong>قند بسیار پایینی</strong>&nbsp;دارد. سرشار از&nbsp;<strong>آهن</strong>&nbsp;است. یک فنجان کامل هندوانه، تنها&nbsp;<strong>۱۰ گرم قند</strong>&nbsp;دارد. تنها اشتباه ما در خوردن هندوانه این است که به یک فنجان راضی نمی&zwnj;شویم!</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h3>۷- طالبی</h3>

<p>طالبی رنگ نارنجی خود را مدیون&nbsp;<strong>ویتامین A&nbsp;</strong>زیاد است. یک فنجان از این ملون خوش طعم حاوی&nbsp;<strong>۱۳ گرم قند</strong>&nbsp;است. این مقدار ممکن است کمی بیشتر از دیگر میوه&zwnj;ها باشد؛ اما این را به خاطر داشته باشید که یک قوطی نوشابه، نزدیک به<strong>&nbsp;۴۰ گرم قند</strong>&nbsp;در خود دارد و مواد مغذی خاصی نیز در آن یافت نمی&zwnj;شود.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h3>۸- هلو</h3>

<p>هلو فوق&zwnj;العاده شیرین است؛ اما یک هلوی متوسط&nbsp;<strong>تنها ۱۳ گرم قند</strong>&nbsp;دارد. به همین دلیل همچنان میوه&zwnj;ای با قند پایین به حساب می&zwnj;آید.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h2>میزان قند کدام میوه ها بالاست؟</h2>

<p>البته تمام میوه&zwnj;ها در مقایسه با میان وعده&zwnj;های قندی سرشار از&nbsp;<strong>ویتامین&zwnj;ها، مواد معدنی</strong>&nbsp;و&nbsp;<strong>فیبرند</strong>. غذاهای سرشار از فیبر گوارش را کند می&zwnj;کنند، این یعنی بعد از خوردن میوه، به صورت ناگهانی، قند خونتان افزایش پیدا نمی&zwnj;کند. با این حال مانند بسیاری از دیگر چیزها در زندگی، اینجا نیز باید همیشه تعادل را رعایت کنید.</p>

<p>&nbsp;</p>

<p>میزان قند این میوه ها نسبتا بالاست:</p>

<p>&nbsp;</p>

<ul>
	<li>آلو برقانی؛</li>
	<li>پرتقال؛</li>
	<li>کیوی؛</li>
	<li>گلابی</li>
	<li>آناناس.</li>
</ul>

<p>&nbsp;</p>

<p><img alt="بعضی از میوه‌ها هم وجود دارند که قند بالایی دارند. " src="https://kermany.com/wp-content/uploads/2021/12/9999.jpg" style="height:500px; width:750px" /></p>

<p>باید حواستان را جمع قند میوه&zwnj;ها کنید.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>این میوه&zwnj;ها بیش از اندازه قند دارند:</p>

<p>&nbsp;</p>

<ul>
	<li>نارنگی؛</li>
	<li>گیلاس؛</li>
	<li>انگور؛</li>
	<li>انار؛</li>
	<li>انبه؛</li>
	<li>انجیر؛</li>
	<li>موز؛</li>
	<li>میوه&zwnj;های خشک، مانند خرما، کشمش، زرد آلوی خشک و آلو.</li>
</ul>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h2>جدول قند میوه ها</h2>

<p>&nbsp;</p>

<p>خوردن میوه تازه، سالم&zwnj;ترین راه برای رضایت پیدا کردن از خوردن چیزی شیرین است. اگر&nbsp;<strong>فعال و سالم</strong>&nbsp;باشید، مصرف بالای&nbsp;<strong>فروکتوز</strong>، قند موجود در میوه، مسلما نمی&zwnj;تواند موجب نگرانی&zwnj;تان شود. با این حال، اگر در خطر دیابت قرار دارید یا در تلاشید مصرف قندتان را کاهش دهید، در جدول زیر میزان قند میوه&zwnj;ها به همراه کالری آن&zwnj;ها درج شده است.</p>

<p>&nbsp;</p>

<table>
	<tbody>
		<tr>
			<td>میوه ( ۸۵ گرم )</td>
			<td>مقدار کلی قند ( گرم )</td>
			<td>مجموع کالری</td>
		</tr>
		<tr>
			<td>تمشک</td>
			<td>۳٫۷</td>
			<td>۴۴</td>
		</tr>
		<tr>
			<td>توت فرنگی</td>
			<td>۴٫۱</td>
			<td>۲۷</td>
		</tr>
		<tr>
			<td>تمشک سیاه</td>
			<td>۴٫۲</td>
			<td>۳۷</td>
		</tr>
		<tr>
			<td>هندوانه</td>
			<td>۵٫۳</td>
			<td>۲۵</td>
		</tr>
		<tr>
			<td>گریپ فروت</td>
			<td>۵٫۹</td>
			<td>۳۶</td>
		</tr>
		<tr>
			<td>طالبی</td>
			<td>۶٫۷</td>
			<td>۲۹</td>
		</tr>
		<tr>
			<td>شلیل</td>
			<td>۶٫۷</td>
			<td>۳۷</td>
		</tr>
		<tr>
			<td>هلو</td>
			<td>۷٫۱</td>
			<td>۳۳</td>
		</tr>
		<tr>
			<td>پرتقال</td>
			<td>۸</td>
			<td>۴۰</td>
		</tr>
		<tr>
			<td>زرد آلو</td>
			<td>۷٫۸</td>
			<td>۴۲</td>
		</tr>
		<tr>
			<td>آلو</td>
			<td>۸٫۴</td>
			<td>۳۹</td>
		</tr>
		<tr>
			<td>گلابی</td>
			<td>۸٫۴</td>
			<td>۴۹</td>
		</tr>
		<tr>
			<td>آناناس</td>
			<td>۸٫۴</td>
			<td>۴۲</td>
		</tr>
		<tr>
			<td>بلوبری</td>
			<td>۸٫۴</td>
			<td>۴۹</td>
		</tr>
		<tr>
			<td>سیب</td>
			<td>۸٫۸</td>
			<td>۴۴</td>
		</tr>
		<tr>
			<td>نارنگی</td>
			<td>۹</td>
			<td>۴۵</td>
		</tr>
		<tr>
			<td>کیوی</td>
			<td>۹٫۲</td>
			<td>۵۶</td>
		</tr>
		<tr>
			<td>موز</td>
			<td>۱۰٫۴</td>
			<td>۷۶</td>
		</tr>
		<tr>
			<td>گیلاس</td>
			<td>۱۰٫۹</td>
			<td>۵۴</td>
		</tr>
		<tr>
			<td>انار</td>
			<td>۱۱٫۶</td>
			<td>۷۰</td>
		</tr>
		<tr>
			<td>انبه</td>
			<td>۱۲٫۷</td>
			<td>۶۰</td>
		</tr>
		<tr>
			<td>انگور</td>
			<td>۱۳٫۸</td>
			<td>۵۷</td>
		</tr>
		<tr>
			<td>انجیر</td>
			<td>۱۳٫۸</td>
			<td>۶۲</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h2>با شاخص گلیسمی آشنا شوید</h2>

<p>&nbsp;</p>

<p>شاخص گلیسمی (GI) یکی از ابزارهای مقایسه&zwnj;ای تغذیه&zwnj;ای است که می&zwnj;توانید از آن برای&nbsp;<strong>ارزیابی کیفیت کربوهیدرات&zwnj;ها</strong>یی استفاده کنید که می&zwnj;خورید. شاخص گلیسمی نشان می&zwnj;دهد که کربوهیدرات&zwnj;های موجود در یک غذای خاص، چقدر سریع بر قند خون شما تأثیر می&zwnj;گذارد. با انتخاب غذاهای با شاخص گلیسمی پایین، می&zwnj;توانید افزایش چشمگیر قند خون خود را به حداقل برسانید.</p>

<p>&nbsp;</p>

<p>اگر غذایی با شاخص گلیسمی بالا مصرف کنید، باید انتظار داشته باشید که قند خونتان، در مدت زمان کم، به حد خطرآفرین برسد. این امر باعث&nbsp;<strong>افزایش سریع قند خون</strong>&nbsp;بعد از غذا می&zwnj;شود. عوامل زیادی می&zwnj;توانند شاخص گلیسمی یک غذا را تغییر دهند. این عوامل شامل ترکیب آن و نحوه پخت غذاست. وقتی غذاها با هم مخلوط می&zwnj;شوند، شاخص گلیسمی غذا نیز تغییر می&zwnj;کند.</p>

<p>&nbsp;</p>

<p>شاخص گلیسمی غذا، بر اساس یک وعده غذایی معمولی از یک غذای خاص نیست. به عنوان مثال، هویج دارای شاخص گلیسمی بالایی است، اما برای به دست آوردن مقدار اندازه گیری شده برای شاخص گلیسمی هویج، باید نیم کیلو هویج بخورید.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h2>میزان شاخص گلیسمی چقدر باید باشد؟</h2>

<p>&nbsp;</p>

<p>برای تعیین شاخص گلیسمی غذاها به یکی از سه دسته تقسیم می&zwnj;شوند: کم، متوسط ​​یا زیاد.</p>

<p>&nbsp;</p>

<ul>
	<li>غذاهای با&nbsp;<strong>GI پایین</strong>&nbsp;دارای شاخص گلیسمی&zwnj;شان&nbsp;<strong>۵۵</strong>&nbsp;یا کمتر است؛</li>
	<li>غذاهای با&nbsp;<strong>GI متوسط</strong>&nbsp;​​بین&nbsp;<strong>۵۶ تا&nbsp;</strong>۶۹&zwnj;اند؛</li>
	<li>غذاهای با&nbsp;<strong>GI بالا</strong>&nbsp;<strong>۷۰</strong>&nbsp;یا بالاترند؛</li>
</ul>

<p>&nbsp;</p>

<p>برای بار گلیسمی زیر ۱۰ کم، ۱۰ تا ۲۰ متوسط ​​و بالای ۲۰ زیاد در نظر گرفته می&zwnj;شود.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h2>چه عواملی بر شاخص گلیسمی تاثیر می&zwnj;گذارد؟</h2>

<p>&nbsp;</p>

<p>هنگام تعیین رتبه گلیسمی یک غذا، چندین عامل در نظر گرفته می&zwnj;شود. این عوامل عبارت&zwnj;اند از:</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h3>۱- اسیدیته</h3>

<p>&nbsp;</p>

<p>غذاهایی که بسیار اسیدی&zwnj;اند، مانند&nbsp;<strong>ترشی&zwnj;ها</strong>، نسبت به غذاهایی شاخص GI کمتری دارند که اسیدی نیستند. این توضیح می&zwnj;دهد که چرا نان&zwnj;های تهیه شده با اسید لاکتیک، مانند&nbsp;<strong>نان خمیر ترش</strong>، نسبت به نان سفید دارای&nbsp;<strong>شاخص GI کمتری&zwnj;</strong>اند.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h3>۲- زمان پخت</h3>

<p>&nbsp;</p>

<p>هرچه یک غذا طولانی&zwnj;تر پخته شود، GI نیز بیشتر می&zwnj;شود. هنگامی پخت غذا، نشاسته یا کربوهیدرات&zwnj;ها شروع به تجزیه شدن می&zwnj;کنند.</p>

<p>&nbsp;</p>

<p><img alt="قند میوه‌ها هنگام پخته شدن شروع به تجزیه‌شدن می‌کنند" src="https://kermany.com/wp-content/uploads/2016/01/2020.jpg" style="height:500px; width:750px" /></p>

<p>با کمپوت کردن میوه&zwnj;ها، قندشان شروع به تجزیه شدن می&zwnj;کنند.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h3>۳- محتوای فیبر</h3>

<p>&nbsp;</p>

<p>به طور کلی، غذاهایی که فیبر بالایی دارند، رتبه&nbsp;<strong>گلیسمی پایین&zwnj;تری</strong>&nbsp;دارند. پوشش&zwnj;های الیافی اطراف&nbsp;<strong>حبوبات</strong>&nbsp;و&nbsp;<strong>دانه&zwnj;ها</strong>&nbsp;به این معنی است که بدن آن&zwnj;ها را آهسته&zwnj;تر تجزیه می&zwnj;کند. بنابراین، آن&zwnj;ها در مقیاس گلیسمی کمتر از غذاهای بدون این پوشش&zwnj;اند، مانند سبوس.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h3>۴- فرآوری</h3>

<p>&nbsp;</p>

<p>به عنوان یک قاعده کلی، هرچه یک غذا بیشتر فرآوری شده باشد، در&nbsp;<strong>مقیاس گلیسمی بالاتر&nbsp;</strong>است. به عنوان مثال،&nbsp;<strong>آب میوه</strong>&nbsp;رتبه<strong>&nbsp;GI بالاتر</strong>ی نسبت به میوه&zwnj;های تازه دارد.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h3>۵- رسیده بودن</h3>

<p>&nbsp;</p>

<p>هر چه میوه یا سبزی رسیده&zwnj;تر باشد، شاخص GI بالاتری دارد. مطمئناً برای هر قانون استثنائاتی وجود دارد، این&zwnj;ها دستورالعمل&zwnj;های کلی&zwnj;اند که هنگام ارزیابی تأثیر بالقوه قند خون یک غذای خاص باید رعایت شوند.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h2>مزایای استفاده از شاخص گلیسمی</h2>

<p>&nbsp;</p>

<p>انتخاب غذاهایی با تأثیر گلیسمی پایین می&zwnj;تواند به پایین نگه داشتن سطح قند خون شما کمک کند. با این حال، همچنین باید به دقت به اندازه&zwnj;های توصیه شده پایبند باشید.</p>

<p>&nbsp;</p>

<p>رتبه بندی گلیسمی فقط برای افراد مبتلا به&nbsp;<a href="https://kermany.com/%D8%AF%DB%8C%D8%A7%D8%A8%D8%AA-%DA%86%DB%8C%D8%B3%D8%AA/">دیابت</a>&nbsp;نیست. کسانی که سعی در کاهش وزن یا کاهش گرسنگی دارند نیز از GI به عنوان یک ابزار رژیم غذایی استفاده می&zwnj;کنند. زیرا می&zwnj;تواند با آن&nbsp;<strong>اشتها</strong>&nbsp;را کنترل کنند. از آنجایی که هضم غذا در بدن بیشتر طول می&zwnj;کشد، فرد می&zwnj;تواند برای مدت طولانی تری احساس سیری کند.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h2>خطرات استفاده از شاخص گلیسمی</h2>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>شاخص گلیسمی به شما کمک می&zwnj;کند تا&nbsp;<strong>کربوهیدرات&zwnj;های با کیفیت بالاتر</strong>&nbsp;را انتخاب کنید. با این حال، این بار کربوهیدرات کل رژیم غذایی شما است که در نهایت بر سطح قند خون تأثیر می&zwnj;گذارد. انتخاب غذاهای کم گلیسمی می&zwnj;تواند کمک کننده باشد؛ اما باید کل کربوهیدرات&zwnj;هایی را مدیریت کنید که می&zwnj;خورید یا می&zwnj;پزید.</p>

<p>&nbsp;</p>

<p>همچنین شاخص گلیسمی ارزش غذایی کلی یک غذا را در نظر نمی&zwnj;گیرد. به عنوان مثال، فقط به این دلیل که پاپ کورن مایکروویو در وسط جدول شاخص گلیسمی غذاها قرار دارد، به این معنی نیست که شما باید فقط با پاپ کورن مایکروویو زندگی کنید.</p>

<p>&nbsp;</p>

<h5>به اندازه بخور، همیشه بخور!</h5>

<p>تو نمی&zwnj;تونی به خاطر لاغری، تا آخر عمر قید میوه رو بزنی! تو باید یاد بگیری چطور و چقدر بخوری تا هم خوش اندام باشی و از هیچ غذایی هم محروم نشی.رژیم دکتر کرمانی یه سبک زندگی جدیده نه یه برنامه غذایی ساده.</p>

<p><a href="https://landing.kermany.com/main/" target="_blank">همین حالا رژیمت رو شروع کن</a></p>

<p><img alt="kermany" src="https://kermany.com/wp-content/themes/kermany/assets/img/call-to-action.png" /></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h2>جدول شاخص گلیسمی و میزان قند میوه ها</h2>

<p>&nbsp;</p>

<p><strong><a href="https://kermany.com/healthy-diet-plan/">رژیم غذایی سالم</a></strong>&nbsp;برای&nbsp;<strong>کنترل دیابت</strong>&nbsp;مهم است.&nbsp;<strong>میوه&zwnj;ها و سبزی&zwnj;ها</strong>&nbsp;بخش مهمی از یک رژیم غذایی سالم&zwnj;اند. دانستن شاخص گلیسمی و همچنین بار گلیسمی برخی از میوه&zwnj;ها و سبزی&zwnj;ها رایج تر، به شما کمک می&zwnj;کند تا میوه&zwnj;های مورد علاقه خود را برای گنجاندن در رژیم غذایی روزانه انتخاب کنید.</p>

<p>&nbsp;</p>

<table>
	<tbody>
		<tr>
			<td>میوه ها</td>
			<td>شاخص گلیسمی (گلوکز = ۱۰۰)</td>
			<td>اندازه به گرم</td>
			<td>بار گلیسمی در هر وعده</td>
		</tr>
		<tr>
			<td>سیب متوسط</td>
			<td>۳۹</td>
			<td>۱۲۰</td>
			<td>۶</td>
		</tr>
		<tr>
			<td>موز رسیده</td>
			<td>۶۲</td>
			<td>۱۲۰</td>
			<td>۱۶</td>
		</tr>
		<tr>
			<td>خرمای خشک</td>
			<td>۴۲</td>
			<td>۸۰</td>
			<td>۱۸</td>
		</tr>
		<tr>
			<td>گریپ فروت</td>
			<td>۲۵</td>
			<td>۱۲۰</td>
			<td>۳</td>
		</tr>
		<tr>
			<td>انگور متوسط</td>
			<td>۵۹</td>
			<td>۱۲۰</td>
			<td>۱۱</td>
		</tr>
		<tr>
			<td>پرتقال متوسط</td>
			<td>۴۰</td>
			<td>۱۲۰</td>
			<td>۴</td>
		</tr>
		<tr>
			<td>هلو متوسط</td>
			<td>۴۲</td>
			<td>۱۲۰</td>
			<td>۵</td>
		</tr>
		<tr>
			<td>کنسرو کم شکر هلو</td>
			<td>۴۰</td>
			<td>۱۲۰</td>
			<td>۵</td>
		</tr>
		<tr>
			<td>گلابی متوسط</td>
			<td>۴۳</td>
			<td>۱۲۰</td>
			<td>۵</td>
		</tr>
		<tr>
			<td>کنسرو گلابی در عصاره گلابی</td>
			<td>۳۸</td>
			<td>۱۲۰</td>
			<td>۴</td>
		</tr>
		<tr>
			<td>آلو، بدون هسته</td>
			<td>۲۹</td>
			<td>۶۰</td>
			<td>۱۰</td>
		</tr>
		<tr>
			<td>کشمش</td>
			<td>۶۴</td>
			<td>۶۰</td>
			<td>۲۸</td>
		</tr>
		<tr>
			<td>هندوانه</td>
			<td>۷۲</td>
			<td>۱۲۰</td>
			<td>۴</td>
		</tr>
		<tr>
			<td>سبزیجات</td>
			<td>شاخص گلیسمی (گلوکز = ۱۰۰)</td>
			<td>اندازه سروینگ (گرم)</td>
			<td>بار گلیسمی در هر وعده</td>
		</tr>
		<tr>
			<td>نخود سبز، متوسط</td>
			<td>۵۱</td>
			<td>۸۰</td>
			<td>۴</td>
		</tr>
		<tr>
			<td>هویج، متوسط</td>
			<td>۳۵</td>
			<td>۸۰</td>
			<td>۲</td>
		</tr>
		<tr>
			<td>زردک</td>
			<td>۵۲</td>
			<td>۸۰</td>
			<td>۴</td>
		</tr>
		<tr>
			<td>سیب زمینی سرخ شده پخته، متوسط</td>
			<td>۱۱۱</td>
			<td>۱۵۰</td>
			<td>۳۳</td>
		</tr>
		<tr>
			<td>سیب زمینی سفید آب پز، متوسط</td>
			<td>۸۲</td>
			<td>۱۵۰</td>
			<td>۲۱</td>
		</tr>
		<tr>
			<td>پوره سیب زمینی فوری، متوسط</td>
			<td>۸۷</td>
			<td>۱۵۰</td>
			<td>۱۷</td>
		</tr>
		<tr>
			<td>سیب زمینی شیرین، متوسط</td>
			<td>۷۰</td>
			<td>۱۵۰</td>
			<td>۲۲</td>
		</tr>
		<tr>
			<td>سیب&zwnj;زمینی پشندی، متوسط</td>
			<td>۵۴</td>
			<td>۱۵۰</td>
			<td>۲۰</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>میزان قند میوه &zwnj;ها از جدول قند میوه ها پیدا کنید و هنگام برنامه&zwnj;ریزی غذایی&zwnj;تان حواستان به آن&zwnj;ها باشد. اگر از شاخص گلیسمی، برای برنامه&zwnj;ریزی وعده&zwnj;های غذایی استفاده کنید، می&zwnj;توانید سطح قند خون خود را بهتر مدیریت کنید.</p>

<p>&nbsp;</p>

<p>با این حال حواستان را جمع کنید را که برای انتخاب یک ماده غذایی کالری، مواد مغذی و شاخص گلیسمی را باهم در نظر بگیرید. مدیریت سطح قند خون، از طریق رژیم غذایی، بخش بسیار مهمی در مدیریت دیابت شما است.</p>
', 1, CAST(N'2021-12-30T00:18:42.0000000' AS DateTime2), N'026ddb7f-8f99-430a-84f7-eb93d7c440ac.jpg', 0)
INSERT [dbo].[Tbl_Article] ([Id], [CategoryId], [Title], [Description], [Text], [Type], [CreateDate], [ImageName], [VisitCount]) VALUES (1003, 1002, N'تا ۳۰ سالگی فرصت جذب کلسیم را دارید!', N'برخی از داروهایی که استفاده می‌کنیم، می‌تواند در از دست دادن توده استخوانی تاثیرگذار باشد. مثلا کورتون. باید از مصرف مکرر و طولانی مدت کورتون خودداری کنیم. برای داشتن استخوان سالم، باید ترکیب‌های اصلی سازنده استخوان‌ها، مانند کلسیم، منیزیم، فسفر و پروتئین‌ها را استفاده کنیم.', N'<p>برخی از داروهایی که استفاده می&zwnj;کنیم، می&zwnj;تواند در از دست دادن توده استخوانی تاثیرگذار باشد. مثلا کورتون. باید از مصرف مکرر و طولانی مدت کورتون خودداری کنیم. برای داشتن استخوان سالم، باید&nbsp;<strong>ترکیب&zwnj;های اصلی سازنده استخوان&zwnj;ها</strong>، مانند&nbsp;<strong>کلسیم، منیزیم، فسفر و پروتئین&zwnj;ها</strong>&nbsp;را استفاده کنیم.</p>

<p><img alt="برای داشتن اسنخوان‌های سالم از منابع کلسیم و منیزیم استفاده کنیم" src="https://kermany.com/wp-content/uploads/2021/12/00000.jpg" style="height:500px; width:750px" /></p>

<p>زود دیر می&zwnj;شود، همین الان به فکر مصرف غذاهای کلسیم&zwnj;دار باشید.</p>

<p>از دوران کودکی و نوجوانی، ذخایر کلسیم بدن را باید افزایش دهیم تا با داشتن استخوان سالم&zwnj;تر، بتوانیم به اندازه کافی فعالیت فیزیکی کنیم. منابع کلسیم شامل&nbsp;<strong>شیر، ماست، دوغ و پنیر</strong>&nbsp;است. بادام و نخود هم منابع خوبی برای کلسیم به حساب می&zwnj;آیند. در حبوبات و مغز دانه&zwnj;های دیگر هم کلسیم وجود دارد.</p>

<p>مصرف منابع پروتئین می&zwnj;تواند سبب استحکام استخوان&zwnj;ها. استفاده از نور خورشید، ویتامین D را به بدن ما می&zwnj;رساند. در ماهی هم، ویتامین D خوبی وجود دارد. منیزیم نیز در سلامت استخوان&zwnj;ها نقش بسزایی دارد. از منابع منیزیم می&zwnj;توان به&nbsp;<strong>سبزی&zwnj;های برگ&zwnj;سبز، حبوبات و مغزها</strong>&nbsp;اشاره کرد. مصرف برخی از مواد غذایی سبب تسریع پوکی استخوان می&zwnj;شود.</p>

<p>باید از مصرف&nbsp;<strong>غذاهای شور</strong>&nbsp;خودداری کرد زیرا مانع جذب کلسیم می شوند .مصرف&nbsp;<strong>کافئین</strong>&nbsp;نیز مانع جذب کلسیم می شود. باید مصرف کافئین را به حداقل برسانیم. زیرا منجر به پوکی استخوان می شود. سطح ویتامین دی بدن نیز نباید از عدد ۳۰ کمتر باشد. باید ببینیم آیا مقدار کلسیمی که در روز استفاده می&zwnj;کنیم کافی است یا &nbsp;به اندازه کافی ویتامین D داریم؟</p>

<p><strong>ورزش</strong>&nbsp;در ساختن توده استخوانی قوی کمک کننده است. نکته&zwnj;های تغذیه&zwnj;ای باید از دوران نوجوانی و جوانی رعایت شود. ذخایر کلسیم بدن باید پر باشد تا بتوان این ذخایر را در طول عمر استفاده کند. بیشترین جذب کلسیم تا ۳۰ سالگی است. البته بعد از آن هم بدن مقداری کلسیم جذب خواهد کرد.</p>

<p>همانطور که گفتیم باید حواستان به داروهایی باشد که با کلسیم میانه خوبی ندارند. باید از مصرف مکرر و طولانی مدت&nbsp;<strong>کورتون</strong>&nbsp;خودداری کنیم. همچنین برخی از داروهایی به استخوان آسیب می&zwnj;رساند که برای تیروئید استفاده می&zwnj;شود. باید حتماً روزانه&nbsp;<strong>۱۵۰ دقیقه فعالیت بدنی</strong>&nbsp;داشته باشیم و به اندازه کافی لبنیات مصرف کنیم.</p>
', 2, CAST(N'2021-12-30T00:47:42.0000000' AS DateTime2), N'1291411c-7367-4195-ae56-a52b4d0538af.jpg', 0)
INSERT [dbo].[Tbl_Article] ([Id], [CategoryId], [Title], [Description], [Text], [Type], [CreateDate], [ImageName], [VisitCount]) VALUES (1004, 1002, N'آیا خوردن قهوه سلامت کلیه‌ها را به خطر می‌اندازد؟', N'محققان جدید چندین متابولیت در خون را شناسایی کردند که سطح آن‌ها با مصرف قهوه تغییر می‌کند. یعنی ممکن است بر خطر ابتلاء به بیماری مزمن کلیوی تأثیر بگذارند.  محققان در این مطالعه، ۳۷۲ متابولیت خون را در ۳۸۱۱ شرکت کننده، بررسی کردند. نتیجه تحقیق‌ها مشخص کرد ۴۱ متابولیت با مصرف قهوه مرتبط‌اند. تیم تحقیق این متابولیت‌ها را در ۱۰۴۳ بزرگسال دیگر، در مطالعه‌ای مربوط به قلب، تجزیه و تحلیل کردند. در این مطالعه مشخص شد۲۰ متابولیت از ۴۱ متابولیت نیز با مصرف قهوه در این گروه ارتباط دارد.', N'<p>محققان جدید چندین متابولیت در خون را شناسایی کردند که سطح آن&zwnj;ها با مصرف قهوه تغییر می&zwnj;کند. یعنی ممکن است بر خطر ابتلاء به بیماری مزمن کلیوی تأثیر بگذارند.</p>

<p>محققان در این مطالعه، ۳۷۲ متابولیت خون را در ۳۸۱۱ شرکت کننده، بررسی کردند. نتیجه تحقیق&zwnj;ها مشخص کرد ۴۱ متابولیت با مصرف قهوه مرتبط&zwnj;اند. تیم تحقیق این متابولیت&zwnj;ها را در ۱۰۴۳ بزرگسال دیگر، در مطالعه&zwnj;ای مربوط به قلب، تجزیه و تحلیل کردند. در این مطالعه مشخص شد۲۰ متابولیت از ۴۱ متابولیت نیز با مصرف قهوه در این گروه ارتباط دارد.</p>

<h2>انواع متابولیت&zwnj;ها کدامند؟</h2>

<p>محققان پی بردند ۳ نوع از این متابولیت&zwnj;ها، خطر ابتلا به بیماری مزمن کلیوی را بیشتر می&zwnj;کنند. این سه نوع شامل:</p>

<ol>
	<li>گلیکوشنودوکسی کولات</li>
	<li>سولفات O-متیل کاتکول</li>
	<li>متیل کاتکول سولفات است</li>
</ol>

<p><img alt="ایا خوردن قهوه روی متابولیسم تاثیر دارد؟" src="https://kermany.com/wp-content/uploads/2021/12/90909.jpg" style="height:500px; width:750px" /></p>

<p>قهوه خوردن باعث تغییر متابولیسم می&zwnj;شود!</p>

<p>گلیکوشنودوکسی&nbsp;کولات، یک لیپید است که در متابولیسم اسید صفراوی اولیه نقش دارد. این لیپید بر اثر&zwnj;های مفید مصرف قهوه بر سلامت کلیه&nbsp;تأثیر&nbsp;می&zwnj;گذارد. سولفات O-متیل&nbsp;کاتکول&nbsp;و ۳-&nbsp;متیل&nbsp;کاتکول&nbsp;سولفات که در متابولیسم بنزوات نگهدارنده نقش دارند، ممکن است نمایانگر جنبه&zwnj;های مضر قهوه روی کلیه&zwnj;ها باشند.</p>

<p>مجموعه بزرگی از شواهد علمی نشان داده است که مصرف مقدار متوسط قهوه با یک رژیم غذایی سالم، خطری برای سلامتی ندارد. اما مصرف بیش از&zwnj;اند ازه آن، سلامتی کلیه را به خطر می&zwnj;اندازد.</p>
', 2, CAST(N'2021-12-30T00:49:03.0000000' AS DateTime2), N'a9e27783-4df1-42b8-9321-a5b1e2de42f3.jpg', 0)
SET IDENTITY_INSERT [dbo].[Tbl_Article] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_ArticleCategory] ON 

INSERT [dbo].[Tbl_ArticleCategory] ([Id], [Title], [IsActive]) VALUES (1002, N'آموزشی', 1)
SET IDENTITY_INSERT [dbo].[Tbl_ArticleCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Slider] ON 

INSERT [dbo].[Tbl_Slider] ([Id], [Imagename], [Url], [IsActive]) VALUES (1, N'a0e77e31-b5d1-4351-9c45-857a62abdd44.jpg', N'/', 1)
INSERT [dbo].[Tbl_Slider] ([Id], [Imagename], [Url], [IsActive]) VALUES (2, N'a6ff96b0-58be-4b3c-a7fd-15130d58ffb6.jpg', N'/', 1)
INSERT [dbo].[Tbl_Slider] ([Id], [Imagename], [Url], [IsActive]) VALUES (3, N'730dc0f5-fc8f-43fb-901e-22113d8695e8.jpg', N'/', 1)
SET IDENTITY_INSERT [dbo].[Tbl_Slider] OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_User] ON 

INSERT [dbo].[Tbl_User] ([Id], [Username], [Password], [Role], [CreateDate], [IsActive]) VALUES (1, N'09105854846', N'E1-0A-DC-39-49-BA-59-AB-BE-56-E0-57-F2-0F-88-3E', 2, CAST(N'2021-12-29T12:42:20.8599447' AS DateTime2), 1)
INSERT [dbo].[Tbl_User] ([Id], [Username], [Password], [Role], [CreateDate], [IsActive]) VALUES (2, N'09105854846', N'82-CD-AA-3B-1C-C6-4A-81-B9-40-AF-B5-93-F7-67-7E', 1, CAST(N'2021-12-30T15:28:21.1218090' AS DateTime2), 1)
INSERT [dbo].[Tbl_User] ([Id], [Username], [Password], [Role], [CreateDate], [IsActive]) VALUES (3, N'09105854854', N'E1-0A-DC-39-49-BA-59-AB-BE-56-E0-57-F2-0F-88-3E', 1, CAST(N'2021-12-30T15:29:21.9978600' AS DateTime2), 1)
SET IDENTITY_INSERT [dbo].[Tbl_User] OFF
GO
/****** Object:  Index [IX_Tbl_Article_CategoryId]    Script Date: 12/30/2021 3:41:56 PM ******/
CREATE NONCLUSTERED INDEX [IX_Tbl_Article_CategoryId] ON [dbo].[Tbl_Article]
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tbl_Article]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Article_Tbl_ArticleCategory_CategoryId] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Tbl_ArticleCategory] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Article] CHECK CONSTRAINT [FK_Tbl_Article_Tbl_ArticleCategory_CategoryId]
GO
USE [master]
GO
ALTER DATABASE [KermanyDb] SET  READ_WRITE 
GO
