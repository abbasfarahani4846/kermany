﻿using Kermany.Data.Entities;

using Microsoft.EntityFrameworkCore;

namespace Kermany.Data
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {

        }

        public DbSet<Tbl_User> Tbl_User { get; set; }
        public DbSet<Tbl_Article> Tbl_Article { get; set; }
        public DbSet<Tbl_ArticleCategory> Tbl_ArticleCategory { get; set; }
        public DbSet<Tbl_Slider> Tbl_Slider { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var cascadeFKs = modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
                fk.DeleteBehavior = DeleteBehavior.Restrict;

            //modelBuilder.Entity<Tbl_User>().HasQueryFilter(u => u.IsDelete == false);

            base.OnModelCreating(modelBuilder);
        }

    }
}
