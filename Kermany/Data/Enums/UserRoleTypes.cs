﻿namespace Kermany.Data.Enums
{
    public enum UserRoleTypes
    {
        User = 1,
        Admin = 2,
    }
}
