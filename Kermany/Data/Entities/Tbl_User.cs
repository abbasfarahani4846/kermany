﻿using System.ComponentModel.DataAnnotations;

using Kermany.Data.Enums;

namespace Kermany.Data.Entities
{
    public class Tbl_User
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "شماره همراه")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string Username { get; set; }
        [Display(Name = "کلمه عبور")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string Password { get; set; }
        [Display(Name = "سطح کاربر")]
        public UserRoleTypes Role { get; set; } = UserRoleTypes.User;
        [Display(Name = "تاریخ ثبت نام")]
        public DateTime CreateDate { get; set; } = DateTime.Now;

        [Display(Name = "فعال / غیر فعال")]
        public bool IsActive { get; set; } = true;
    }
}
