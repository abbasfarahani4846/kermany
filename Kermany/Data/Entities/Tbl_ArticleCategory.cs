﻿using System.ComponentModel.DataAnnotations;

namespace Kermany.Data.Entities
{
    public class Tbl_ArticleCategory
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "عنوان")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string Title { get; set; }
        [Display(Name = "فعال / غیر فعال")]
        public bool IsActive { get; set; }
    }
}
