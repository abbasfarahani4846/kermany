﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Kermany.Data.Enums;

namespace Kermany.Data.Entities
{
    public class Tbl_Article
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "دسته بندی")]
        public int CategoryId { get; set; }
        [Display(Name = "عنوان")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string Title { get; set; }
        [Display(Name = "توضیح مختصر")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string Description { get; set; }
        [Display(Name = "متن")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        [AllowHtml]
        public string Text { get; set; }
        [Display(Name = "نوع مقاله")]
        public ArticleTypes Type { get; set; } = ArticleTypes.Article;
        [Display(Name = "تاریخ درج")]
        public DateTime CreateDate { get; set; } = DateTime.Now;
        [Display(Name = "تصویر")]
        public string ImageName { get; set; } = "nophoto.png";
        [Display(Name = "تعداد بازدید")]
        public int VisitCount { get; set; } = 0;


        public virtual Tbl_ArticleCategory Category { get; set; }
    }
}
