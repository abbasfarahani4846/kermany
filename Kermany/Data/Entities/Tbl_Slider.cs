﻿using System.ComponentModel.DataAnnotations;

namespace Kermany.Data.Entities
{
    public class Tbl_Slider
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "تصویر")] public string Imagename { get; set; } = "";
        [Display(Name = "لینک")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string Url { get; set; }
        [Display(Name = "فعال / غیر فعال")]
        public bool IsActive { get; set; }
    }
}
