﻿using System.ComponentModel.DataAnnotations;

namespace Kermany.Data.ViewModel
{
    public class LoginViewModel
    {
        [Display(Name = "نام کاربری")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]

        public string Username { get; set; }
        [Display(Name = "کلمه عبور")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]

        public string Password { get; set; }
    }
}
