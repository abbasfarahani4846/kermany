﻿using Microsoft.AspNetCore.Mvc;

namespace Kermany.Controllers
{
    public class HomeController : Controller
    {
        [HttpPost]
        [Route("file-upload")]
        public IActionResult UploadImage(IFormFile upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            if (upload.Length <= 0) return null;

            var fileName = Guid.NewGuid() + Path.GetExtension(upload.FileName).ToLower();

            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/EditorUpload", fileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                upload.CopyTo(stream);

            }

            var url = $"{"/EditorUpload/"}{fileName}";
            return Json(new { uploaded = true, url });
        }
    }
}
