﻿using Kermany.Data;
using Kermany.Data.Entities;
using Kermany.Data.ViewModel;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using ShopFile.Core.Security;

namespace Kermany.Pages
{
    public class RegisterModel : PageModel
    {
        private ApplicationContext _context;

        public RegisterModel(ApplicationContext context)
        {
            _context = context;
        }
        public void OnGet()
        {
        }
        [BindProperty]
        public RegisterViewModel register { get; set; }
        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = _context.Tbl_User.FirstOrDefault(d =>
                d.Username == register.Username);

            if (user != null)
            {
                ViewData["message"] = "کاربری با این مشخصات وجود دارد";
                return Page();
            }

            _context.Tbl_User.Add(new Tbl_User()
            {
                IsActive = true,
                Password = PasswordHelper.EncodePasswordMd5(register.Password),
                Username = register.Username
            });
            _context.SaveChanges();

            return Redirect("/login");
        }


    }
}
