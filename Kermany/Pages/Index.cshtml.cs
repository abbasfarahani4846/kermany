﻿using Kermany.Data;
using Kermany.Data.Enums;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Kermany.Pages
{
    public class IndexModel : PageModel
    {
        private ApplicationContext _context;

        public IndexModel(ApplicationContext context)
        {
            _context = context;
        }

        public void OnGet()
        {
            ViewData["last_articles"] = _context.Tbl_Article.Where(d => d.Type == ArticleTypes.Article).OrderByDescending(d => d.CreateDate).Take(7).ToList();
            ViewData["slides"] = _context.Tbl_Slider.Where(d => d.IsActive).ToList();
            ViewData["last_regim"] = _context.Tbl_Article.Where(d => d.Type == ArticleTypes.Regime).Take(9).ToList();
        }
    }
}