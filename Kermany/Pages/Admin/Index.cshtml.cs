using Kermany.Data;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ShopFile.Core.Security;

namespace Kermany.Pages.Admin
{
    [PermissionChecker]
    public class IndexModel : PageModel
    {
        private ApplicationContext _context;

        public IndexModel(ApplicationContext context)
        {
            _context = context;
        }
        public void OnGet()
        {
            ViewData["article_count"] = _context.Tbl_Article.Count();
            ViewData["user_count"] = _context.Tbl_User.Count();
            ViewData["category_count"] = _context.Tbl_ArticleCategory.Count();


            ViewData["last_users"] = _context.Tbl_User.OrderByDescending(d => d.CreateDate).Take(10).ToList();
            ViewData["best_article"] = _context.Tbl_Article.OrderByDescending(d => d.VisitCount).Take(10).ToList();
        }
    }
}
