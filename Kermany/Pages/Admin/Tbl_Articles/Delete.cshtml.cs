﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Kermany.Data;
using Kermany.Data.Entities;
using ShopFile.Core.Security;

namespace Kermany.Pages.Admin.Tbl_Articles
{
    [PermissionChecker]
    public class DeleteModel : PageModel
    {
        private readonly Kermany.Data.ApplicationContext _context;

        public DeleteModel(Kermany.Data.ApplicationContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Tbl_Article Tbl_Article { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tbl_Article = await _context.Tbl_Article
                .Include(t => t.Category).FirstOrDefaultAsync(m => m.Id == id);

            if (Tbl_Article == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tbl_Article = await _context.Tbl_Article.FindAsync(id);

            if (Tbl_Article != null)
            {
                if (Tbl_Article.ImageName != "nophoto.png")
                {
                    //حذف عکس اصلی
                    if (System.IO.File.Exists(Path.Combine(Directory.GetCurrentDirectory() + "/wwwroot/Article/" +
                                                           Tbl_Article.ImageName)))
                    {
                        System.IO.File.Delete(Path.Combine(Directory.GetCurrentDirectory() + "/wwwroot/Article/" +
                                                           Tbl_Article.ImageName));
                    }
                }

                _context.Tbl_Article.Remove(Tbl_Article);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
