﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Kermany.Data;
using Kermany.Data.Entities;
using ShopFile.Core.Security;

namespace Kermany.Pages.Admin.Tbl_Articles
{
    [PermissionChecker]
    public class IndexModel : PageModel
    {
        private readonly Kermany.Data.ApplicationContext _context;

        public IndexModel(Kermany.Data.ApplicationContext context)
        {
            _context = context;
        }

        public IList<Tbl_Article> Tbl_Article { get;set; }

        public async Task OnGetAsync()
        {
            Tbl_Article = await _context.Tbl_Article
                .Include(t => t.Category).ToListAsync();
        }
    }
}
