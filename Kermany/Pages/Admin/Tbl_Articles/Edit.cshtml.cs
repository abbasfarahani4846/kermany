﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Kermany.Data;
using Kermany.Data.Entities;
using ShopFile.Core.Security;

namespace Kermany.Pages.Admin.Tbl_Articles
{
    [PermissionChecker]
    public class EditModel : PageModel
    {
        private readonly Kermany.Data.ApplicationContext _context;

        public EditModel(Kermany.Data.ApplicationContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Tbl_Article Tbl_Article { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tbl_Article = await _context.Tbl_Article
                .Include(t => t.Category).FirstOrDefaultAsync(m => m.Id == id);

            if (Tbl_Article == null)
            {
                return NotFound();
            }
            ViewData["CategoryId"] = new SelectList(_context.Tbl_ArticleCategory, "Id", "Title");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(IFormFile imagename)
        {
            //if (!ModelState.IsValid)
            //{
            //    return Page();
            //}

            if (imagename != null)
            {
                if (Tbl_Article.ImageName != "nophoto.png")
                {
                    //حذف عکس اصلی
                    if (System.IO.File.Exists(Path.Combine(Directory.GetCurrentDirectory() + "/wwwroot/Article/" +
                                                 Tbl_Article.ImageName)))
                    {
                        System.IO.File.Delete(Path.Combine(Directory.GetCurrentDirectory() + "/wwwroot/Article/" +
                                                 Tbl_Article.ImageName));
                    }
                }

                Tbl_Article.ImageName = Guid.NewGuid() + Path.GetExtension(imagename.FileName);

                //ذخیره تصویر اصلی
                string ImagePath = Path.Combine(Directory.GetCurrentDirectory() + "/wwwroot/Article/" +
                                                Tbl_Article.ImageName);
                using (var stream = new FileStream(ImagePath, FileMode.Create))
                {
                    imagename.CopyTo(stream);
                }
            }

            _context.Attach(Tbl_Article).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Tbl_ArticleExists(Tbl_Article.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool Tbl_ArticleExists(int id)
        {
            return _context.Tbl_Article.Any(e => e.Id == id);
        }
    }
}
