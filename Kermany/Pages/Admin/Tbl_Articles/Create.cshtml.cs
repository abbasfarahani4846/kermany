﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Kermany.Data;
using Kermany.Data.Entities;
using ShopFile.Core.Security;

namespace Kermany.Pages.Admin.Tbl_Articles
{
    [PermissionChecker]
    public class CreateModel : PageModel
    {
        private readonly Kermany.Data.ApplicationContext _context;

        public CreateModel(Kermany.Data.ApplicationContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            ViewData["CategoryId"] = new SelectList(_context.Tbl_ArticleCategory, "Id", "Title");
            return Page();
        }

        [BindProperty]
        public Tbl_Article Tbl_Article { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync(IFormFile imagename)
        {
            //Tbl_Article.Category = new Tbl_ArticleCategory();
            //if (!ModelState.IsValid)
            //{
            //    ViewData["CategoryId"] = new SelectList(_context.Tbl_ArticleCategory, "Id", "Title");
            //    return Page();
            //}

            if (imagename != null)
            {
                Tbl_Article.ImageName = Guid.NewGuid() + Path.GetExtension(imagename.FileName);

                //ذخیره تصویر اصلی
                string ImagePath = Path.Combine(Directory.GetCurrentDirectory() + "/wwwroot/Article/" +
                                                Tbl_Article.ImageName);
                using (var stream = new FileStream(ImagePath, FileMode.Create))
                {
                    imagename.CopyTo(stream);
                }
            }


            _context.Tbl_Article.Add(Tbl_Article);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
