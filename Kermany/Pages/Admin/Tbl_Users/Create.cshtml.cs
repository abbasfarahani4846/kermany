﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Kermany.Data;
using Kermany.Data.Entities;
using ShopFile.Core.Security;

namespace Kermany.Pages.Admin.Tbl_Users
{
    [PermissionChecker]
    public class CreateModel : PageModel
    {
        private readonly Kermany.Data.ApplicationContext _context;

        public CreateModel(Kermany.Data.ApplicationContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Tbl_User Tbl_User { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Tbl_User.Password = PasswordHelper.EncodePasswordMd5(Tbl_User.Password);

            _context.Tbl_User.Add(Tbl_User);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
