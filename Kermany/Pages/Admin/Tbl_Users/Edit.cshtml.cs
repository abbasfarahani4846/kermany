﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Kermany.Data;
using Kermany.Data.Entities;
using ShopFile.Core.Security;

namespace Kermany.Pages.Admin.Tbl_Users
{
    [PermissionChecker]
    public class EditModel : PageModel
    {
        private readonly Kermany.Data.ApplicationContext _context;

        public EditModel(Kermany.Data.ApplicationContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Tbl_User Tbl_User { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tbl_User = await _context.Tbl_User.FirstOrDefaultAsync(m => m.Id == id);

            if (Tbl_User == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(string newPassword = "")
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (string.IsNullOrEmpty(newPassword) == false)
            {
                Tbl_User.Password = PasswordHelper.EncodePasswordMd5(newPassword);
            }
            _context.Attach(Tbl_User).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Tbl_UserExists(Tbl_User.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool Tbl_UserExists(int id)
        {
            return _context.Tbl_User.Any(e => e.Id == id);
        }
    }
}
