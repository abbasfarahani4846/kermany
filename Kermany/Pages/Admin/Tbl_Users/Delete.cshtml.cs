﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Kermany.Data;
using Kermany.Data.Entities;
using ShopFile.Core.Security;

namespace Kermany.Pages.Admin.Tbl_Users
{
    [PermissionChecker]
    public class DeleteModel : PageModel
    {
        private readonly Kermany.Data.ApplicationContext _context;

        public DeleteModel(Kermany.Data.ApplicationContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Tbl_User Tbl_User { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tbl_User = await _context.Tbl_User.FirstOrDefaultAsync(m => m.Id == id);

            if (Tbl_User == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tbl_User = await _context.Tbl_User.FindAsync(id);

            if (Tbl_User != null)
            {
                _context.Tbl_User.Remove(Tbl_User);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
