﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Kermany.Data;
using Kermany.Data.Entities;
using ShopFile.Core.Security;

namespace Kermany.Pages.Admin.Tbl_Sliders
{
    [PermissionChecker]
    public class CreateModel : PageModel
    {
        private readonly Kermany.Data.ApplicationContext _context;

        public CreateModel(Kermany.Data.ApplicationContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Tbl_Slider Tbl_Slider { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync(IFormFile imagename)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (imagename != null)
            {

                Tbl_Slider.Imagename = Guid.NewGuid() + Path.GetExtension(imagename.FileName);

                //ذخیره تصویر اصلی
                string ImagePath = Path.Combine(Directory.GetCurrentDirectory() + "/wwwroot/Slider/" +
                                                Tbl_Slider.Imagename);
                using (var stream = new FileStream(ImagePath, FileMode.Create))
                {
                    imagename.CopyTo(stream);
                }
            }

            _context.Tbl_Slider.Add(Tbl_Slider);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
