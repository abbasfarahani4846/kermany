using Kermany.Data;
using Kermany.Data.Entities;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Kermany.Pages
{
    public class SingleArticleModel : PageModel
    {
        private ApplicationContext _context;

        public SingleArticleModel(ApplicationContext context)
        {
            _context = context;
        }

        public Tbl_Article Article { get; set; }
        public List<Tbl_Article> relatedArticle { get; set; } = new List<Tbl_Article>();
        public IActionResult OnGet(int id = 0)
        {
            var article = _context.Tbl_Article.FirstOrDefault(d => d.Id == id);
            if (article == null)
            {
                return Redirect("/");
            }

            Article = article;

            article.VisitCount += 1;
            _context.Update(article);
            _context.SaveChanges();

            relatedArticle = _context.Tbl_Article.Where(d => d.CategoryId == article.CategoryId && d.Id != article.Id).OrderByDescending(d => d.CreateDate).Take(5).ToList();
            return Page();
        }
    }
}
