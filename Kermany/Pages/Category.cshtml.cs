using Kermany.Data;
using Kermany.Data.Entities;
using Kermany.Data.Enums;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Kermany.Pages
{
    public class CategoryModel : PageModel
    {
        private ApplicationContext _context;

        public CategoryModel(ApplicationContext context)
        {
            _context = context;
        }

        public List<Tbl_Article> Articles { get; set; }
        public List<Tbl_Slider> Slides { get; set; }
        public void OnGet(int id)
        {
            Articles = _context.Tbl_Article.Where(d => d.CategoryId == id && d.Type == ArticleTypes.Article).ToList();
            Slides = _context.Tbl_Slider.Where(d => d.IsActive).ToList();
        }
    }
}
