﻿using System.Security.Claims;

using Kermany.Data;
using Kermany.Data.Enums;
using Kermany.Data.ViewModel;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using ShopFile.Core.Security;

namespace Kermany.Pages
{
    public class LoginModel : PageModel
    {
        private ApplicationContext _context;

        public LoginModel(ApplicationContext context)
        {
            _context = context;
        }
        public void OnGet()
        {
        }
        [BindProperty]
        public LoginViewModel login { get; set; }
        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = _context.Tbl_User.FirstOrDefault(d =>
                d.Username == login.Username && d.Password == PasswordHelper.EncodePasswordMd5(login.Password) && d.IsActive);

            if (user == null)
            {
                ViewData["message"] = "کاربری با این مشخصات پیدا نشد";
                return Page();
            }

            #region Login Code And Properties

            //کد ها و تنظیمات مربوط به لاگیم کردن کاربر
            var clims = new List<Claim>()
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Username.ToString()),
                new Claim(ClaimTypes.Role, user.Role.ToString()),
            };

            var identity = new ClaimsIdentity(clims, CookieAuthenticationDefaults.AuthenticationScheme);

            var principal = new ClaimsPrincipal(identity);

            var properties = new AuthenticationProperties
            {
                IsPersistent = true
            };

            //ست کردن کوکیه لاگین
            HttpContext.SignInAsync(principal, properties);

            #endregion


            if (user.Role == UserRoleTypes.Admin)
            {
                return Redirect("/Admin");
            }
            else
            {
                return Redirect("/");
            }

        }
    }
}
