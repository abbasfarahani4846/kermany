﻿using Kermany.Data;
using Kermany.Data.Enums;

using Microsoft.AspNetCore.Mvc;

namespace Kermany.Components
{
    public class MenuComponent : ViewComponent
    {
        private ApplicationContext _context;

        public MenuComponent(ApplicationContext context)
        {
            _context = context;
        }

        public Task<IViewComponentResult> InvokeAsync()
        {
            var lstRegime = _context.Tbl_Article.Where(d => d.Type == ArticleTypes.Regime).OrderBy(d => d.CreateDate)
                .ToList();

            var lstCategory = _context.Tbl_ArticleCategory.Where(d => d.IsActive).ToList();

            return Task.FromResult((IViewComponentResult)View("Menu", Tuple.Create(lstCategory, lstRegime)));
        }


    }
}
